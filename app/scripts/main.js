$(document).ready(function () {

  $('.x-welcome').slick({
    arrows: true,
    dots: true,
    autoplay: true,
    speed: 500
  });

  $('.x-fresh__cards').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  });

  $('.x-best__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  });

  $('.x-news__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  });

  $('.x-header__toggle').click(function () {
    $('.x-header__nav').slideToggle('fast', function () {
      // Callback
    });
  });

  $('.c-slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    fade: true,
    asNavFor: '.c-slider-nav'
  });

  $('.c-slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.c-slider-for',
    dots: false,
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.callme').click(function () {
    $('.c-wrapper').toggleClass('c-wrapper_active');
  });

  $('.c-popup__close').click(function () {
    $('.c-wrapper').toggleClass('c-wrapper_active');
  });

  $('.play').click(function () {
    $(this).toggle();
    $(this).next().toggle();
    $(this).prev()[0].play();
  });

  $('.pause').click(function () {
    $(this).toggle();
    $(this).prev().prev()[0].pause();
    $(this).prev().toggle();
  });

  $('.track').each(function (index, el) {
    init($(this));
  });

  function init(card) {
    card.children('.cover').append('<button class="play"></button><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100"><path id="circle" fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M50,2.9L50,2.9C76,2.9,97.1,24,97.1,50v0C97.1,76,76,97.1,50,97.1h0C24,97.1,2.9,76,2.9,50v0C2.9,24,24,2.9,50,2.9z"/></svg>');

    var audio = card.find('audio'),
      play = card.find('.play'),
      circle = card.find('#circle'),
      getCircle = circle.get(0),
      totalLength = getCircle.getTotalLength();

    circle.attr({
      'stroke-dasharray': totalLength,
      'stroke-dashoffset': totalLength
    });

    play.on('click', function () {
      if (audio[0].paused) {
        $('audio').each(function (index, el) {
          $('audio')[index].pause();
        });
        $('.track').removeClass('playing');
        audio[0].play();
        card.addClass('playing');
      } else {
        audio[0].pause();
        card.removeClass('playing');
      }
    });

    audio.on('timeupdate', function () {
      var currentTime = audio[0].currentTime,
        maxduration = audio[0].duration,
        calc = totalLength - (currentTime / maxduration * totalLength);

      circle.attr('stroke-dashoffset', calc);
    });

    audio.on('ended', function () {
      card.removeClass('playing');
      circle.attr('stroke-dashoffset', totalLength);
    });

  }

  $('.phone').mask('+7(999)999-99-99');

  jcf.replaceAll();

});